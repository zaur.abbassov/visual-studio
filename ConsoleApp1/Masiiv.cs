﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Masiiv
    {
        public static void Mas()
        {
            var arvud = new int[10];
            arvud[3] = 77;
            arvud[1] = 100;
            int[] teine = arvud;
            arvud = new int[10];
            teine[3]++;
            Console.WriteLine(teine[3]+teine[1]);

            int[] teine2 = new int[] { 1, 2, 7, 3, 6, 8, 11 };
            int[] kolmas =  { 1, 2, 7, 3, 6, 8, 11 };
            Console.WriteLine(teine2[3]);

            //tsukkli susteem raske 
            int[] yhestKumneni = Enumerable.Range(1, 10).ToArray(); 
            Console.WriteLine(string.Join(", ", yhestKumneni));

            object[] eriTyypi = new object[3];//erityypi andmed
            eriTyypi[0] = "";
            eriTyypi[1] = "Zaur";
            eriTyypi[2] = 64;
            Console.WriteLine(string.Join(", ",eriTyypi));

            int[][] misSeeOn = new int[3][];
            misSeeOn[0] = new int[] { 1, 2, 3 };
            misSeeOn[1] = new int[] { 1, 2 };
            misSeeOn[2] = new int[] { 3, 4, 5, 6 };
            int[][] misSeeon = { new int[] { 1, 23 }, new int[] { 1, 2, 3 }, new int[] { 7, 4, 2, 1, 3, 2 } };
            int[,] table = new int[8, 8];
            int[,] table2 = { { 1, 2, 3 }, { 2, 3, 1 } };

        }
    }
}
