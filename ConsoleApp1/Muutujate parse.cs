﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Muutujate_parse
    {
        public static void muutuja()
        {
            Console.WriteLine("\n \nKolmapaev");
            //Teisendamine andme muutujad
            int lyhike = 500000000;

            Console.WriteLine($"Muutuja lühike väärtus on {lyhike} ");
            long pikk = lyhike;
            short teine = (short)(pikk + 7);

            //1. oli n'ide Casting nimetusega (tüüp)(avaldis) näided saad vaadata eespool

            //teisendamine stringiks ja stringist naide on all
            string s = teine.ToString();
            Console.WriteLine(s);

            s = string.Format("See asi on {0}", 4 + 7 * teine);
            Console.WriteLine(s);
            Console.WriteLine($"See asi on { 4 + 7 * teine}");
            //String Format kasutab placeholdarite kohal to stringi
            //WriteLine kasutrab string.Formatid
            //string.Format kasutab ToString()
            //$-string avaldis kasutab sisemiselt string.Formatit

            //ToString on alles JÄÄMÄE veepealne osa
            //mõned näited veel
            double d = Math.PI;
            Console.WriteLine(d);

            d = 1.0 / 7;
            Console.WriteLine("{0:F2}", d);
            string Nimi = "Zaur;Artur;Hillar";
            string[] andmed = Nimi.Split(';');


            foreach (string item in andmed)
            {
                Console.WriteLine(item + " " + andmed.Length);
            }
            //suvaline andmetüüp (arv, kuupaev, kellaaeg) toString() - teisendab stringiks
            // kuidas saab vastupidi

            Console.Write("mis paeval oled sundind oled: ");
            //DateTime sunnipaev = DateTime.Parse(Console.ReadLine());
            DateTime sunnipaev = DateTime.Parse("23.03.1979");
            Console.WriteLine(sunnipaev);
            Console.WriteLine($"Sa oled siis {((DateTime.Today - sunnipaev).TotalDays / 365)}");

            double testDouble = double.Parse("44,89");
            Console.WriteLine(testDouble);

            Console.WriteLine("Sisesta kuupaeva ");
            if (DateTime.TryParse("21.01.1986", out sunnipaev))
            {
                Console.WriteLine(sunnipaev);
            }
            else
            {
                Console.WriteLine("Sellist kuupaeva pole olemas");
            }

            //2. Stringist  ja stringiks 
            //  parse
            //  tryparse
            //

            //3. Convert

            Console.WriteLine("Anna uks arv: ");
            int arv = Convert.ToInt32("1");
            Console.WriteLine(arv);
            decimal Arv2, tulumaks;

            do
            {
                Console.WriteLine("Sisesta palk ? ");
            } while (decimal.TryParse("2000", out Arv2) == false);

            if (Arv2 > 500)
            {
                tulumaks = Arv2 - (Arv2 - 500) / 5;
                Console.WriteLine($"Teie tulumaks katte on: {Arv2 - tulumaks}");
            }
            else
            {
                Console.WriteLine(Arv2);
            }
        }
    }
}
