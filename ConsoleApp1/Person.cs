﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Person
    {
        public string Nimi;
        public string Perenimi;
        public string Isikukood;

        public DateTime SynniPaev()
        {
            var IVanus = Isikukood.Substring(1, 6).ToString();
            return new DateTime(int.Parse(IVanus.Substring(0, 2).ToString()) + 1900, int.Parse(IVanus.Substring(2, 2).ToString()), int.Parse(IVanus.Substring(4, 2).ToString()));
        }

        public int Vanus()
        {
            var Desk = (DateTime.Today - SynniPaev()).Days / 365;
            return Desk;
        }

        public string Sugu()
        {

            return int.Parse(Isikukood.Substring(0, 1)) % 2 == 0 ? "naine" : "mees";
        }

        

    }
    
}
