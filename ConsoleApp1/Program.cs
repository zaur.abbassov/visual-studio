﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Komentaar
            //Console.WriteLine("Hello World 12");

            //int arv;
            //do
            //    Console.WriteLine("Sisesta arvud ");
            //while (int.TryParse(Console.ReadLine(), out arv) != true);

            //arv += 8;
            //Console.WriteLine(arv);

            //byte Test = byte.MaxValue;
            //Console.WriteLine(++Test);

            //string Nimi = "Maria";
            //int Vanus = 19;


            //Console.WriteLine("Tere, {1} ja tema vanus on {0}",Nimi, Vanus);

            //DateTime.Now.DayOfWeek =
            #endregion
            #region Trenni palaan

            var td = DateTime.Now.DayOfWeek;


            //switch (td)
            //{
            //    case DayOfWeek.Saturday:
            //        Console.WriteLine("Joome olut ja saunas");
            //        break;
            //    case DayOfWeek.Sunday:
            //        Console.WriteLine("Joome olut");
            //        break;
            //    case DayOfWeek.Wednesday:
            //        Console.WriteLine("lahen trenni");
            //        break;
            //    default:
            //        Console.WriteLine("too paev");
            //        break;
            //}

            #endregion

            #region Kolmapäev 


            // Muutujate_parse.muutuja();

            // Masiiv.Mas();
            // Tsykkel.Tsykk();


            #endregion


            #region Neljapaev
            //int arv = 52;
            //int[] mas = new int[arv];
            //Random r = new Random();
            //for (int i = 0; i < mas.Length; i++)
            //{
            //    mas[i] = i;
            //}
            //for (int i = 0; i < mas.Length; i++)
            //{
            //    int temp = 0;
            //    arv = r.Next(52);
            //    temp = mas[i];
            //    mas[i] = mas[arv];
            //    mas[arv] = temp;
            //}

            //for (int i = 0; i < mas.Length; i++)
            //{
            ////    Console.Write($"{mas[i]}{(i % 4 == 3 ? "\n" : "\t")}");
            //}


            #endregion

            #region Reede
            // ReedeneUL.UL1();
            // ReedeneUL.UL2();
            //  ReedeneUL.UL3();
            #endregion

            #region Teisenädala esmaspäev
            //int[] m1 = new int[5];
            //int[] m2 = new int[5] { 1, 2, 3, 4, 5 };
            //int[] m3 = new int[] { 1, 2, 3, 4, 5 };
            //int[] m4 = { 1, 2, 3, 4, 5 };

            //List<int> l1 = new List<int>();
            //List<int> l2 = new List<int>() { 1, 2, 3, 4, 5 };
            //List<int> l3 = new List<int> { 1, 2, 3, 4, 5 };

            //Inimene zaur = new Inimene();

            //zaur.Nimi = "Zaur Abbassov";
            //zaur.Vanus = 33;

            //Console.WriteLine(zaur);

            //Inimene Maria = new Inimene()
            //{
            //    Nimi = "Maria Stsukina",
            //    Vanus = 32
            //};

            //Console.WriteLine(Maria);

            //zaur.Kaasa = Maria;
            //zaur.Lapsed.Add(new Inimene { Nimi = "Matou", Vanus = 11 });


            //Inimene Teine = zaur;
            //Teine.Nimi = "Test";

            //Console.WriteLine(zaur);

            //var kolmas = new Inimene
            //{
            //    Nimi = "Keegi kolmas",
            //    Vanus = 1
            //};

            //var neljas = new Inimene { Nimi = "Ants", Vanus = 40 };

            //SpordiPaev sp1 = new SpordiPaev { Nimi = "Zaur", TeePikkus = 1100, Aeg = 10 };
            //SpordiPaev sp2 = new SpordiPaev { Nimi = "Andres", TeePikkus = 1100, Aeg = 12 };
            //SpordiPaev sp3 = new SpordiPaev { Nimi = "Maria", TeePikkus = 1100, Aeg = 13 };

            //var lis = new List<SpordiPaev>();
            //lis.Add(sp1);
            //lis.Add(sp2);
            //lis.Add(sp3);

            //foreach(var x in lis)
            //{
            //    Console.WriteLine(x);
            //}

            ////sp1.Kiirus();

            ////Tests();

            //Console.WriteLine();

            //var Zaur = new Person() { Nimi = "Zaur", Perenimi = "Abbassov", Isikukood = "38601212229" };
            //Console.WriteLine(Zaur.SynniPaev());


            #endregion

            #region Teinenädala Teiesipäev

            NewInimene ini1;
            ini1 = new NewInimene { Nimi = "Henn", Kaasa = new NewInimene { Nimi = "Maris", Kaasa = new NewInimene { Nimi = "Henn" } } };
            NewInimene ini2;
            ini2 = new NewInimene();

            Console.WriteLine(ini1);
            Console.WriteLine(ini1.Kaasa.Kaasa?.Nimi??"Kaasat veel ei ole");//ei anna veateada ilma ? annab
            //Console.WriteLine(ini1.Kaasa.Kaasa.Vanus);

            //NewInimene xini = null;
            //Console.WriteLine(xini);

            var x = ini1?.Vanus;
            int? arv1 = 0;
            int? arv2 = 0;
            var tulemus = arv1 ?? 0 + arv2 ?? 0;
            Nullable <int> arv3 = null;
            #endregion

            Console.ReadKey();
        }

    }
}
