﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp1
{
    class ReedeneUL
    {
        public static void UL1()
        {
            decimal[] arv1 = new decimal[2];
            Random r = new Random();




            decimal arv = 0;
            int mark = 0;
            int Vastus2 = 0;

            for (int a = 0; a < 10; a++)
            {
                bool Controll = true;

                mark = r.Next(4) + 1;

                for (int i = 0; i < arv1.Length; i++)
                {
                    arv1[i] = r.Next(10) + 1;
                }

                //for (int j = 0; j < 5; j++)
                {

                    for (int j = 0; j < 5; j++)
                    {
                        do

                            switch (mark)
                            {
                                case 1:
                                    Console.WriteLine("Palju on :" + arv1[0] + " + " + arv1[1] + " =");
                                    break;
                                case 2:
                                    Console.WriteLine("Palju on :" + arv1[0] + " - " + arv1[1] + " =");
                                    break;
                                case 3:
                                    Console.WriteLine("Palju on :" + arv1[0] + " / " + arv1[1] + " =");
                                    break;
                                case 4:
                                    Console.WriteLine("Palju on :" + arv1[0] + " * " + arv1[1] + " =");
                                    break;
                            }


                        while (decimal.TryParse(Console.ReadLine(), out arv) == false);

                        switch (mark)
                        {
                            case 1:
                                if ((arv1[0] + arv1[1]) == arv)
                                {
                                    Console.WriteLine("Teie vastus on Oige");
                                    Vastus2 = Vastus2 + 5;
                                    Controll = true;
                                }
                                else
                                {
                                    Console.WriteLine("Teie vastus on Vale");
                                    Vastus2 = Vastus2 - 1;
                                    Controll = false;
                                }
                                break;
                            case 2:
                                if ((arv1[0] - arv1[1]) == arv)
                                {
                                    Console.WriteLine("Teie vastus on Oige");
                                    Vastus2 = Vastus2 + 5;
                                    Controll = true;
                                }
                                else
                                {
                                    Console.WriteLine("Teie vastus on Vale");
                                    Vastus2 = Vastus2 - 1;
                                    Controll = false;
                                }
                                break;
                            case 3:
                                if (Math.Round((arv1[0] / arv1[1]), 2) == arv)
                                {
                                    Console.WriteLine("Teie vastus on Oige");
                                    Vastus2 = Vastus2 + 5;
                                    Controll = true;
                                }
                                else
                                {
                                    Console.WriteLine("Teie vastus on Vale");
                                    Vastus2 = Vastus2 - 1;
                                    Controll = false;
                                }
                                break;
                            case 4:
                                if ((arv1[0] * arv1[1]) == arv)
                                {
                                    Console.WriteLine("Teie vastus on Oige");
                                    Vastus2 = Vastus2 + 5;
                                    Controll = true;
                                }
                                else
                                {
                                    Console.WriteLine("Teie vastus on Vale");
                                    Vastus2 = Vastus2 - 1;
                                    Controll = false;
                                }

                                break;
                        }
                        if (Controll == true) break;
                    }
                }

            }

            Console.WriteLine("Teie punktid kokku " + Vastus2);
        }

        public static void UL2()
        {
            int[,] mas = new int[10, 10];
            int arv;


            Random r = new Random();
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    mas[i, j] = r.Next(100);

                    Console.Write($"{mas[i, j]:00}{(j % 10 == 9 ? "\n" : " ")}");
                }
            }

            Console.WriteLine();
            bool Control = true;
            do
            {
                Console.WriteLine("Sisesta arvu ");
            }
            while (int.TryParse(Console.ReadLine(), out arv) != true);

            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    if (arv == mas[i, j])
                    {
                        Console.WriteLine("Sellised Arvud on |{0,-4}|{1,-4}|", i + 1, j + 1);
                        Control = false;
                    }
                }
            }

            if (Control == true)
            {
                Console.WriteLine("Sellis arvu pole massiivis ");
            }


        }

        public static void UL3()
        {
            var text = File.ReadAllText(@"C:\Users\zaura\source\repos\ConsoleApp1\ConsoleApp1\palgad.txt");
            Console.WriteLine(text);

            ////string vastus = Console.ReadLine().Substring(0, 1).ToLower();

            //switch(vastus)
            //{
            //    case "v":// vaatmine
            //        break;
            //    case "m":// muutmine
            //        break;
            //    case "l":// lisamine
            //        break;
            //    case "k":// kokku votmisel
            //        break;
            //    default:
            //            Console.WriteLine("sellist tegevust ei ole !!! ");
            //        break;

            //}

            
        }
    }
}
