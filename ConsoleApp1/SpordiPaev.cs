﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class SpordiPaev
    {
        public string Nimi;
        public int TeePikkus;
        public int Aeg;
        
        List<SpordiPaev> l1 = new List<SpordiPaev>();

        public override string ToString() => $"Jooksja {Nimi} labitud teepikkus {TeePikkus} ja aega kulus  {Aeg}";

        public void TrukkiTulemused()
        {
            Console.WriteLine($"{Nimi} jooksis {TeePikkus} meetrid ajaga {Aeg}");
        }

        public int Kiirus()
        {
            int num;
            do
            {
                Console.WriteLine("Sisesta Andmed");
            } while (int.TryParse(Console.ReadLine(), out num) == false);
            return num;
        }
        
       
    }
}
