﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Teine_Nadal
    {
    }
    class Inimene
    {
        private int _InimesteArv = 0;

        public int Number => ++_InimesteArv;
        public string Nimi;
        public string Perenimi;
        public int Vanus;

        public Inimene Kaasa;
        public List<Inimene> Lapsed = new List<Inimene>();

        public override string ToString() => $"{Nimi} vanusega {Vanus}";

        
    }
}
