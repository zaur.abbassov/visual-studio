﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Tsykkel
    {
        public static void Tsykk()
        {
            int[] arvud = new int[10];
            for (int i = 1; i < arvud.Length; i++)
            {
                Console.Write("{0,-4}",arvud[i] = i * i);            
            }

            Console.WriteLine();
            for(int i=0;i<=10;i++)
            {
                Console.Write("{0,3}", i % 10);
            }

            Console.WriteLine("\n_________________________________________");

            String[] Nimi = { "Anna", "Ukku", "Anu", "Maarika", "Joosep", "Andres", "Katri", "Kadri", "Andrus", "Renee" };

            DateTime[] synna = new DateTime[10];
            synna[0] = new DateTime(9, 9, 21);
            synna[1] = new DateTime(11, 11, 2);
            synna[2] = new DateTime(12, 10, 11);
            synna[3] = new DateTime(13, 8, 10);
            synna[4] = new DateTime(14, 1, 5);
            synna[5] = new DateTime(8, 11, 24);
            synna[6] = new DateTime(11, 5, 10);
            synna[7] = new DateTime(12, 6, 19);
            synna[8] = new DateTime(13, 3, 15);
            synna[9] = new DateTime(1, 2, 5);

            DateTime noorem = synna[0], vanem = synna[0];
            int item1=0, item2=0;

            for (int i = 0; i < Nimi.Length; i++)
            {

                if (noorem > synna[i])
                {
                    item1 = i;
                    noorem = synna[i];
                }
                if (vanem < synna[i])
                {
                    item2 = i;
                    vanem = synna[i];
                }
                Console.WriteLine("Tere {0} sinu vanus on {1:0}", Nimi[i], ((DateTime.Today - synna[i]).Days)/365 -2000);

            }

            Console.WriteLine("Koige noorem {0}ja koige vanem {1}", Nimi[item1], Nimi[item2]);
            Console.WriteLine("\n___________________________________________________________");

            Random r = new Random();
            int[] mas = new int[100];
            int temp = 0;
            //int suurim, vaiksem;

            for (int i = 0; i < mas.Length; i++)
            {
                
                mas[i] = r.Next(100);
                temp += mas[i];
                Console.Write($"{mas[i]} { (i % 10 == 9 ? "\n " : " ")}");

            }

            Console.WriteLine("\n___________________________________________________________");
            //while foreach adn do while

            int MassiiviSuurus = 100;

            
            int[] s = new int[MassiiviSuurus];
            int[] sa = new int[MassiiviSuurus];

            for (int i = 0; i < s.Length; i++)
            {
                int arv = r.Next(100);
                s[i] = arv;
                sa[arv]++;
            }

            for (int i = 0; i < s.Length; i++)
            {
                Console.Write("||{0,-3}||", s[i]);
                if (i % 10 == 9)
                {
                    Console.WriteLine();
                }
                else
                {
                    Console.Write(" ");
                }
            }

            Console.WriteLine();
            Console.WriteLine("Maximum:" + s.Max());
            Console.WriteLine("Minimum:" + s.Min());
            Console.WriteLine("Average:" + s.Average());

            Console.WriteLine();

            for (int i = 0; i < sa.Length; i++)
            {
                Console.Write("||{0,3}{1,3}||", i, sa[i]);
                if (i % 10 == 9)
                {
                    Console.WriteLine();
                }
                else
                {
                    Console.Write(" ");
                }
            }

        }
    }
}
